// const express = require("express")
import express from "express"; //framework to create api
import cors from "cors";
import mongoose from "mongoose";
import {userRouter} from './routes/users.js'
import { recipeRouter } from "./routes/recipes.js";
const app = express(); //generate version api
app.use(express.json()); //convert data from frontend to json
app.use(cors());
app.use("/auth",userRouter)
app.use("/recipes",recipeRouter)
mongoose.connect(
  "mongodb+srv://yesminefrikha:mern123456@cluster0.2co7tvl.mongodb.net/Cluster0?retryWrites=true&w=majority"
);
app.listen(3001, () => console.log("SERVER STARTED!"));
