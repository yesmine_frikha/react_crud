import './App.css';
import {BrowserRouter as Router ,Routes,Route} from 'react-router-dom'
import {Home} from './pages/home'
import { Auth } from './pages/auth';
import {CreateRecipe} from './pages/create-recipe'
import  {SavedRecipes}   from "./pages/saved-recipes";
import {Navbar} from "./components/Navbar"
function App() {
  return (
    <div className="App">
      {/* determine where in our app where going to have route*/}
      <Router>
         <Navbar/>
         <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/auth' element={<Auth/>}/>
          <Route path='/create-recipe' element={<CreateRecipe/>}/>
          <Route path='/saved-recipe' element={<SavedRecipes/>}/>
         </Routes>
      </Router>
    </div>
  );
}

export default App;
