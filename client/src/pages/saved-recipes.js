import { useEffect, useState } from "react";
import axios from "axios";
import { useGetUserId } from "../hooks/useGetuserID";
export const SavedRecipes = () => {
  const [savedrecipes,  setSavedRecipes] = useState([]);
  const userID = useGetUserId();
 
  useEffect(() => {
    const fetchSavedRecipe = async () => {
      try {
        const response = await axios.get(
          `http://localhost:3001/recipes/savedRecipes/${userID}`
        );
        setSavedRecipes(response.data.savedRecipes);
        console.log(response.data.savedRecipes);
      } catch (err) {
        console.error(err);
      }
    };
    fetchSavedRecipe();
  }, []);

  return (
    <div>
      <h1>Saved  Recipes </h1>
      <ul>
        {savedrecipes.map((recipe) => (
          <li key={recipe._id}>
            
            <div>
              <h2>{recipe.name} </h2>
             
            </div>
            <div className="instructions">
              <p> {recipe.instructions}</p>
            </div>
            <img src={recipe.imageUrl} alt={recipe.name} />
            <p>Coking Time: {recipe.cookingTime}(minutes)</p>
          </li>
        ))}
      </ul>
    </div>
  );
};
